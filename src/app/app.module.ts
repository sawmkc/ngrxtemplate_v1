import { NgModule } from "@angular/core";

import { RoutesModule } from "./routes/routes.module";
import { SharedModule } from "./shared/shared.module";
import { LayoutModule } from "./layout/layout.module";

import { AppComponent } from "./app.component";

import { StoreModule, MetaReducer } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { StoreRouterConnectingModule, RouterStateSerializer } from "@ngrx/router-store";

//for dev mode
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { storeFreeze } from "ngrx-store-freeze";

import { environment } from "../environments/environment";

import { reducers, effects, CustomSerializer } from "./store";

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RoutesModule,
		LayoutModule,
		SharedModule,
		StoreModule.forRoot(reducers, { metaReducers }),
		!environment.production ? StoreDevtoolsModule.instrument() : [],
		EffectsModule.forRoot(effects),
		StoreRouterConnectingModule
	],
	providers: [{ provide: RouterStateSerializer, useClass: CustomSerializer }],
	bootstrap: [AppComponent]
})
export class AppModule {}
