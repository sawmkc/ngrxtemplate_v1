import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

@NgModule({
	imports: [CommonModule, HttpClientModule, ReactiveFormsModule],
	declarations: [],
	exports: [CommonModule, HttpClientModule, RouterModule, ReactiveFormsModule]
})
export class SharedModule {}
