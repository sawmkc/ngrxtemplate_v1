// Generated by https://quicktype.io

export interface Albums {
	album_group: any;
	album_type: any;
	artists: Artist[];
	available_markets: string[];
	external_urls: any;
	href: string;
	id: string;
	images: Image[];
	name: string;
	release_date: string;
	release_date_precision: any;
	total_tracks: number;
	type: any;
	uri: string;
}

export interface Artist {
	external_urls: any;
	href: string;
	id: string;
	name: string;
	type: string;
	uri: string;
}

export interface Image {
	height: number;
	url: string;
	width: number;
}
