import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Artist } from "./../models/Artist";
import { TOKEN, SERVER, IDS } from "./../../../core/config/serverConfig";
@Injectable()
export class SpotifyService {
	constructor(private _httpClient: HttpClient) {}

	setHeader(): HttpHeaders {
		const headers = new HttpHeaders().set("Content-Type", "application/json").set("authorization", `Bearer ${TOKEN}`);

		return headers;
	}

	getAtists() {
		return this._httpClient.get<Artist[]>(`${SERVER}/artists?ids=${IDS}`, { headers: this.setHeader() });
	}

	getAlbumsById(artistId) {
		return this._httpClient.get<Artist[]>(`${SERVER}/artists/${artistId}/albums`, { headers: this.setHeader() });
	}
}
