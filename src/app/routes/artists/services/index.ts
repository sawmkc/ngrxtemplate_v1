import { SpotifyService } from "./spotify.service";

export const services: any[] = [SpotifyService];

export * from "./spotify.service";
