import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { Routes, RouterModule } from "@angular/router";

import { reducers, effects } from "./store";

//Containers
import * as fromContainers from "./containers";

//Components
import * as fromComponents from "./components";

// Services
import * as fromServices from "./services";

// Guards
import * as formGuards from "./guards";

import { SharedModule } from "src/app/shared/shared.module";

// routes
export const ROUTES: Routes = [
	{
		path: "artists",
		canActivate: [formGuards.ArtistsGuard],
		component: fromContainers.ArtistsComponent
	},
	{
		path: "albums/:id",
		canActivate: [formGuards.AlbumsGuard],
		component: fromContainers.AlbumsListComponent
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(ROUTES),
		StoreModule.forFeature("artists", reducers),
		EffectsModule.forFeature(effects),
		SharedModule
	],
	providers: [...fromServices.services, ...formGuards.guards],
	declarations: [...fromContainers.containers, ...fromComponents.components],
	exports: [...fromContainers.containers, ...fromComponents.components]
})
export class ArtistsModule {}
