import { Component, OnInit, Input } from "@angular/core";

@Component({
	selector: "app-artist",
	templateUrl: "./artist.component.pug",
	styleUrls: ["./artist.component.scss"]
})
export class ArtistComponent implements OnInit {
	@Input()
	artist;
	constructor() {}

	ngOnInit() {
		console.log(this.artist);
	}
}
