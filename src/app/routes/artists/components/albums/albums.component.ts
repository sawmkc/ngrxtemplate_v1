import { Component, OnInit, Input } from "@angular/core";

@Component({
	selector: "app-album",
	templateUrl: "./albums.component.pug",
	styleUrls: ["./albums.component.scss"]
})
export class AlbumsComponent implements OnInit {
	@Input()
	album;
	constructor() {}

	ngOnInit() {}
}
