import { ArtistComponent } from "./artist/artist.component";
import { AlbumsComponent } from "./albums/albums.component";

export const components: any[] = [ArtistComponent, AlbumsComponent];

export * from "./artist/artist.component";
export * from "./albums/albums.component";
