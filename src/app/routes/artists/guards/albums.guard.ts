import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate } from "@angular/router";

import { Store } from "@ngrx/store";
import { Albums } from "../models/Albums";

import * as fromStore from "../store";

import { Observable, of } from "rxjs";
import { tap, filter, take, switchMap, map, catchError } from "rxjs/operators";

@Injectable()
export class AlbumsGuard implements CanActivate {
	constructor(private store: Store<fromStore.ArtistsState>) {}

	canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
		const id = route.params.id;
		return this.checkStore(id).pipe(
			switchMap(() => of(true)),
			catchError(() => of(false))
		);
	}

	checkStore(id): Observable<boolean> {
		return this.store.select(fromStore.getAlbumsLoaded).pipe(
			tap(loaded => {
				if (!loaded) {
					this.store.dispatch(new fromStore.LoadAlbums(id));
				}
			}),
			filter(loaded => loaded),
			take(1)
		);
	}
}
