import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";

import { Store } from "@ngrx/store";

import * as fromStore from "../store";
import { Observable, of } from "rxjs";
import { filter, take, tap, switchMap, catchError } from "rxjs/operators";

@Injectable()
export class ArtistsGuard implements CanActivate {
	constructor(private store: Store<fromStore.ArtistsState>) {}

	canActivate(): Observable<boolean> {
		return this.checkStore().pipe(
			switchMap(() => of(true)),
			catchError(() => of(false))
		);
	}

	checkStore(): Observable<boolean> {
		return this.store.select(fromStore.getArtistsLoaded).pipe(
			tap(loaded => {
				if (!loaded) {
					this.store.dispatch(new fromStore.LoadArtists());
				}
			}),
			filter(loaded => loaded),
			take(1)
		);
	}
}
