import { ArtistsGuard } from "./artists.guard";
import { AlbumsGuard } from "./albums.guard";

export const guards: any[] = [ArtistsGuard, AlbumsGuard];

export * from "./artists.guard";
export * from "./albums.guard";
