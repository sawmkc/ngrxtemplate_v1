import { ArtistsComponent } from "./artists/artists.component";
import { AlbumsListComponent } from "./albums-list/albums-list.component";

export const containers: any[] = [ArtistsComponent, AlbumsListComponent];

export * from "./artists/artists.component";
export * from "./albums-list/albums-list.component";
