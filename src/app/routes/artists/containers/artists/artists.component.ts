import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Artist } from "../../models/Artist";

import { Store } from "@ngrx/store";

import * as fromStore from "../../store";

@Component({
	selector: "app-artists",
	templateUrl: "./artists.component.pug",
	styleUrls: ["./artists.component.scss"]
})
export class ArtistsComponent implements OnInit {
	artists$: Observable<Artist[]>;

	constructor(private _store: Store<fromStore.ArtistsState>) {}

	ngOnInit() {
		this.artists$ = this._store.select(fromStore.getAllArtists);
	}

	trackByFn(index, item) {
		if (!item) return null;
		return item.id;
	}
}
