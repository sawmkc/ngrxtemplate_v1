import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import * as formStore from "../../store";
import { Store } from "@ngrx/store";

@Component({
	selector: "app-albums-list",
	templateUrl: "./albums-list.component.pug",
	styleUrls: ["./albums-list.component.scss"]
})
export class AlbumsListComponent implements OnInit {
	albums$: Observable<any>;
	constructor(private store: Store<formStore.ArtistsState>) {}

	ngOnInit() {
		this.albums$ = this.store.select(formStore.getAllAlbums);
	}

	trackByFn(index, item) {
		if (!item) return null;
		return item.id;
	}
}
