import { ActionReducerMap, createFeatureSelector } from "@ngrx/store";

import * as fromArtists from "./artists.reducer";
import * as fromAlbums from "./albums.reducer";

export interface ArtistsState {
	artists: fromArtists.ArtistState;
	albums: fromAlbums.AlbumsState;
}

export const reducers: ActionReducerMap<ArtistsState> = {
	artists: fromArtists.reducer,
	albums: fromAlbums.reducer
};

export const getArtistsState = createFeatureSelector<ArtistsState>("artists");
