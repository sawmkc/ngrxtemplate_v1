import * as fromArtists from "./../actions/artists.action";
import { Artist } from "./../../models/Artist";

export interface ArtistState {
	entities: { [id: number]: Artist };
	loaded: boolean;
	loading: boolean;
}

export const initialState: ArtistState = {
	entities: {},
	loaded: false,
	loading: false
};

export function reducer(state = initialState, action: fromArtists.ArtistsAction): ArtistState {
	switch (action.type) {
		case fromArtists.LOAD_ARTISTS: {
			return {
				...state,
				loading: true
			};
		}
		case fromArtists.LOAD_ARTISTS_SUCCESS: {
			const artists = action.payload["artists"];
			const entities = artists.reduce(
				// tslint:disable-next-line:no-shadowed-variable
				(entities: { [id: number]: Artist }, artist: Artist) => {
					return {
						...entities,
						[artist.id]: artist
					};
				},
				{
					...state.entities
				}
			);

			return {
				...state,
				loading: false,
				loaded: true,
				entities
			};
		}
		case fromArtists.LOAD_ARTISTS_FAIL: {
			return {
				...state,
				loading: false,
				loaded: false
			};
		}
	}
	return state;
}

export const getArtistsEntities = (state: ArtistState) => state.entities;
export const getArtistsLoading = (state: ArtistState) => state.loading;
export const getArtistsLoaded = (state: ArtistState) => state.loaded;
