import * as fromAlbums from "./../actions/albums.action";
import { Albums } from "./../../models/Albums";

export interface AlbumsState {
	entities: { [id: string]: Albums };
	loaded: boolean;
	loading: boolean;
}

export const initialState: AlbumsState = {
	entities: {},
	loaded: false,
	loading: false
};

export function reducer(state = initialState, action: fromAlbums.AlbumsAction) {
	switch (action.type) {
		case fromAlbums.LOAD_ALBUMS: {
			const artistId = action.id;
			return {
				...state,
				artistId,
				loading: true
			};
		}
		case fromAlbums.LOAD_ALBUMS_SUCCESS: {
			const albums = action.payload;

			const entities = albums.reduce(
				// tslint:disable-next-line:no-shadowed-variable
				(entities: { [id: string]: Albums }, album: Albums) => {
					return {
						...entities,
						[album.id]: album
					};
				},
				{
					...state.entities
				}
			);

			return {
				...state,
				loading: false,
				loaded: true,
				entities
			};
		}
		case fromAlbums.LOAD_ALBUMS_FAIL: {
			return {
				...state,
				loading: false,
				loaded: false
			};
		}
	}
	return state;
}

export const getAlbumEntities = (state: AlbumsState) => state.entities;
export const getAlbumLoading = (state: AlbumsState) => state.loading;
export const getAlbumtsLoaded = (state: AlbumsState) => state.loaded;
