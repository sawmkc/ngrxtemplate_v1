import { Injectable } from "@angular/core";

import { Effect, Actions } from "@ngrx/effects";

import * as artistsActions from "../actions/artists.action";

import { switchMap, map, catchError } from "rxjs/operators";
import { SpotifyService } from "../../services";
import { of } from "rxjs";

@Injectable()
export class ArtistsEffects {
	constructor(private actions$: Actions, private _spotifyService: SpotifyService) {}

	@Effect()
	loadArtists$ = this.actions$.ofType(artistsActions.LOAD_ARTISTS).pipe(
		switchMap(() => {
			return this._spotifyService.getAtists().pipe(
				map(artists => new artistsActions.LoadArtistsSuccess(artists)),
				catchError(error => of(new artistsActions.LoadArtistsFail(error)))
			);
		})
	);
}
