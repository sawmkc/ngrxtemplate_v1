import { Injectable } from "@angular/core";
import { Actions, Effect } from "@ngrx/effects";

import * as albumActions from "./../actions/albums.action";

import { SpotifyService } from "../../services";
import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

@Injectable()
export class AlbumsEffects {
	constructor(private actions$: Actions, private _spotifyService: SpotifyService) {}

	@Effect()
	loadAlbums$ = this.actions$.ofType(albumActions.LOAD_ALBUMS).pipe(
		map(res => res),
		switchMap(type => {
			return this._spotifyService.getAlbumsById(type["id"]).pipe(
				map(albums => new albumActions.LoadAlbumsSuccess(albums["items"])),
				catchError(error => of(new albumActions.LoadAlbumsFail(error)))
			);
		})
	);
}
