import { ArtistsEffects } from "./artists.effect";
import { AlbumsEffects } from "./albums.effect";

export const effects: any[] = [ArtistsEffects, AlbumsEffects];

export * from "./artists.effect";

export * from "./albums.effect";
