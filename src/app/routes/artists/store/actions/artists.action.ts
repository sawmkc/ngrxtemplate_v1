import { Action } from "@ngrx/store";
import { Artist } from "../../models/Artist";

//load artist
export const LOAD_ARTISTS = "[Artists] Load Artists";
export const LOAD_ARTISTS_FAIL = "[Artists] Load Artists Fail";
export const LOAD_ARTISTS_SUCCESS = "[Artists] Load Artists Success";

export class LoadArtists implements Action {
	readonly type = LOAD_ARTISTS;
}

export class LoadArtistsFail implements Action {
	readonly type = LOAD_ARTISTS_FAIL;
	constructor(public payload: any) {}
}

export class LoadArtistsSuccess {
	readonly type = LOAD_ARTISTS_SUCCESS;
	constructor(public payload: Artist[]) {}
}

export type ArtistsAction = LoadArtists | LoadArtistsFail | LoadArtistsSuccess;
