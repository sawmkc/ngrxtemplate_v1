import { Action } from "@ngrx/store";
import { Albums } from "../../models/Albums";

export const LOAD_ALBUMS = "[Albums] Load albums";
export const LOAD_ALBUMS_FAIL = "[Albums] Load albums fail";
export const LOAD_ALBUMS_SUCCESS = "[Albums] Load albums success";

export class LoadAlbums implements Action {
	readonly type = LOAD_ALBUMS;
	constructor(public id: string[]) {}
}

export class LoadAlbumsFail implements Action {
	readonly type = LOAD_ALBUMS_FAIL;
	constructor(public payload: any[]) {}
}

export class LoadAlbumsSuccess implements Action {
	readonly type = LOAD_ALBUMS_SUCCESS;
	constructor(public payload: Albums[]) {}
}

export type AlbumsAction = LoadAlbums | LoadAlbumsFail | LoadAlbumsSuccess;
