import { createSelector } from "@ngrx/store";

import { Artist } from "./../../models/Artist";

import * as fromFeature from "../reducers";
import * as formArtists from "../reducers/artists.reducer";

export const getArtistState = createSelector(
	fromFeature.getArtistsState,
	(state: fromFeature.ArtistsState) => state.artists
);

export const getArtistsEntites = createSelector(getArtistState, formArtists.getArtistsEntities);

export const getAllArtists = createSelector(getArtistsEntites, entities => {
	return Object.keys(entities).map(id => {
		return entities[id];
	});
});

export const getArtistsLoaded = createSelector(getArtistState, formArtists.getArtistsLoaded);
export const getArtistsLoading = createSelector(getArtistState, formArtists.getArtistsLoading);
