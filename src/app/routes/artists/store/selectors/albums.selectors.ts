import { createSelector } from "@ngrx/store";

import * as fromFeature from "../reducers";
import * as fromRoot from "../../../../store";
import * as fromAlbums from "../reducers/albums.reducer";

import { Albums } from "../../models/Albums";

export const getAlbumsState = createSelector(
	fromFeature.getArtistsState,
	(state: fromFeature.ArtistsState) => state.albums
);

export const getAlbumsEntities = createSelector(getAlbumsState, fromAlbums.getAlbumEntities);

export const getAlbumsByArtist = createSelector(
	getAlbumsEntities,
	fromRoot.getRouterState,
	(enities, router): Albums => {
		return router.state && enities[router.state.params.id];
	}
);

export const getAllAlbums = createSelector(getAlbumsEntities, entities => {
	return Object.keys(entities).map(id => {
		return entities[id];
	});
});

export const getAlbumsLoaded = createSelector(getAlbumsState, fromAlbums.getAlbumtsLoaded);

export const getAlbumsLoading = createSelector(getAlbumsState, fromAlbums.getAlbumLoading);
