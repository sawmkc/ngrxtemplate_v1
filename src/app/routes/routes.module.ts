import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ROUTES } from "./routes";

@NgModule({
	imports: [RouterModule.forRoot(ROUTES)],
	declarations: [],
	exports: [RouterModule]
})
export class RoutesModule {}
