import { Routes } from "@angular/router";
import { LayoutComponent } from "../layout/layout.component";

// routes
export const ROUTES: Routes = [
	{
		path: "",
		component: LayoutComponent,
		children: [
			{ path: "", redirectTo: "artists", pathMatch: "full" },
			{ path: "", loadChildren: "../routes/artists/artists.module#ArtistsModule" }
		]
	},
	{ path: "**", redirectTo: "" }
];
