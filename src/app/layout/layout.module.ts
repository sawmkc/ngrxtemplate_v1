import { NgModule } from "@angular/core";
import { LayoutComponent } from "./layout.component";
import { NavbarComponent } from "./navbar/nvabar.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
	imports: [SharedModule],
	declarations: [LayoutComponent, NavbarComponent],
	exports: [LayoutComponent, NavbarComponent],
	providers: []
})
export class LayoutModule {}
